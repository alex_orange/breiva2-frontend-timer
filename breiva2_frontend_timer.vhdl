library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity breiva2_frontend_timer is
    generic (
        clk_freq_hz : positive := 50e6 -- Clock frequency in hertz
    );
    port (
        ext_reset : in std_logic;
        clk_50mhz : in std_logic;
        -- Note: All switches are debounced except for sw3, see schematic
        sw : in std_logic_vector(7 downto 0);
        exp_io : inout std_logic_vector(39 downto 1);
--        exp_in : in std_logic_vector(8 downto 1);
        led : out std_logic_vector(7 downto 0);
        -- uart
        rx : IN std_logic;
        tx : OUT std_logic;
        cts_n : OUT std_logic;
        dsr_n : OUT std_logic;
        dcd_n : OUT std_logic
    );
end breiva2_frontend_timer;

architecture simple of breiva2_frontend_timer is
    constant clks_per_us : positive := clk_freq_hz / 1e6;

    signal clk_5mhz : std_logic := '0';
    signal reset_5s : std_logic := '1';

    signal radio_rx_indication: std_logic;

    signal LNA_on : std_logic;
    signal PA_on : std_logic;
    signal SW_rx : std_logic;
    signal SW_tx : std_logic;

    signal clk : std_logic;

    type frontend_state_t is (
        FRONTEND_STATE_RX,
        TRAN_TO_TX_LNA_TURN_OFF,
        TRAN_TO_TX_SWITCH_TO_TX,
        TRAN_TO_TX_PA_TURN_ON,
        FRONTEND_STATE_TX,
        TRAN_TO_RX_PA_TURN_OFF,
        TRAN_TO_RX_SWITCH_TO_RX,
        TRAN_TO_RX_LNA_TURN_ON);

    signal frontend_state : frontend_state_t := TRAN_TO_RX_SWITCH_TO_RX;
    signal state_count : integer := 10 * clks_per_us;
begin
    exp_io(1) <= clk_50mhz;

    clk <= clk_50mhz;
    radio_rx_indication <= exp_io(2);
    exp_io(3) <= not LNA_on;
    exp_io(4) <= PA_on;
    exp_io(5) <= SW_rx;
    exp_io(6) <= SW_tx;

    clk_5mhz_process:
    process(clk)
        constant divider : natural := 5;
        variable counter : natural := 0;
    begin
        if clk'event and clk = '1' then
            counter := counter + 1;
            if counter = divider then
                counter := 0;
                clk_5mhz <= not clk_5mhz;
            end if;
        end if;
    end process clk_5mhz_process;

    reset_5s_process:
    process(clk)
        constant reset_count : natural := 5 * clk_freq_hz;
        variable counter : natural := 0;
    begin
        if clk'event and clk = '1' then
            if counter < reset_count then
                counter := counter + 1;
            end if;

            if counter < reset_count then
                reset_5s <= '1';
            else
                reset_5s <= '0';
            end if;
        end if;
    end process reset_5s_process;

    attenuator:
    entity work.attenuator(attenuator_arch)
        port map (
            clk_5mhz => clk_5mhz,
            reset => reset_5s,
            attenuation => "1000000",
            ATT_SI => exp_io(7),
            ATT_LE => exp_io(8),
            ATT_CLK => exp_io(9)
                 );

    break_before_make_proc:
    process(clk)
        -- table from VNA:
        --  10 us PA low (duration 15 us)
        --  12 us switch low (duration 12 us)
        --  13 us LNA low (duration 10 us)
        --  23 us LNA high (calculated)
        --  24 us switch high
        --  25 us PA high

        constant LNA_off_delay : integer := 1 * clks_per_us;
        constant LNA_on_delay : integer := 1 * clks_per_us; -- Enumerate this somewhere

        constant PA_off_delay : integer := 2 * clks_per_us;
        constant PA_on_delay : integer := 1 * clks_per_us;

        constant rx_to_tx_switch_time : integer := 1 * clks_per_us;
        constant tx_to_rx_switch_time : integer := 1 * clks_per_us;

        variable last_input : std_logic;

        variable pre_sw_in2 : std_logic := '0';
        variable pre_sw_in : std_logic := '0';
        variable rx_indication : std_logic := '0';
    begin
        if clk'event and clk = '1' then
            -- Doing this to handle metastability

            rx_indication := pre_sw_in;
            pre_sw_in := last_input;
            last_input := radio_rx_indication;

            if state_count /= 0 then
                state_count <= state_count - 1;
            end if;

            case frontend_state is
                when FRONTEND_STATE_RX =>
                    if rx_indication = '0' then
                        state_count <= LNA_off_delay;
                        frontend_state <= TRAN_TO_TX_LNA_TURN_OFF;
                    else
                        frontend_state <= FRONTEND_STATE_RX;
                    end if;

                when TRAN_TO_TX_LNA_TURN_OFF =>
                    if rx_indication = '0' then
                        if state_count = 0 then
                            state_count <= rx_to_tx_switch_time;
                            frontend_state <= TRAN_TO_TX_SWITCH_TO_TX;
                        else
                            frontend_state <= TRAN_TO_TX_LNA_TURN_OFF;
                        end if;
                    else
                        state_count <= LNA_on_delay;
                        frontend_state <= TRAN_TO_RX_LNA_TURN_ON;
                    end if;

                when TRAN_TO_TX_SWITCH_TO_TX =>
                    if rx_indication = '0' then
                        if state_count = 0 then
                            state_count <= PA_on_delay;
                            frontend_state <= TRAN_TO_TX_PA_TURN_ON;
                        else
                            frontend_state <= TRAN_TO_TX_SWITCH_TO_TX;
                        end if;
                    else
                        state_count <= tx_to_rx_switch_time;
                        frontend_state <= TRAN_TO_RX_SWITCH_TO_RX;
                    end if;

                when TRAN_TO_TX_PA_TURN_ON =>
                    if rx_indication = '0' then
                        if state_count = 0 then
                            frontend_state <= FRONTEND_STATE_TX;
                        else
                            frontend_state <= TRAN_TO_TX_PA_TURN_ON;
                        end if;
                    else
                        state_count <= PA_off_delay;
                        frontend_state <= TRAN_TO_RX_PA_TURN_OFF;
                    end if;

                when FRONTEND_STATE_TX =>
                    if rx_indication = '1' then
                        state_count <= PA_off_delay;
                        frontend_state <= TRAN_TO_RX_PA_TURN_OFF;
                    else
                        frontend_state <= FRONTEND_STATE_TX;
                    end if;

                when TRAN_TO_RX_PA_TURN_OFF =>
                    if rx_indication = '1' then
                        if state_count = 0 then
                            state_count <= tx_to_rx_switch_time;
                            frontend_state <= TRAN_TO_RX_SWITCH_TO_RX;
                        else
                            frontend_state <= TRAN_TO_RX_PA_TURN_OFF;
                        end if;
                    else
                        state_count <= PA_on_delay;
                        frontend_state <= TRAN_TO_TX_PA_TURN_ON;
                    end if;

                when TRAN_TO_RX_SWITCH_TO_RX =>
                    if rx_indication = '1' then
                        if state_count = 0 then
                            state_count <= LNA_on_delay;
                            frontend_state <= TRAN_TO_RX_LNA_TURN_ON;
                        else
                            frontend_state <= TRAN_TO_RX_SWITCH_TO_RX;
                        end if;
                    else
                        state_count <= rx_to_tx_switch_time;
                        frontend_state <= TRAN_TO_TX_SWITCH_TO_TX;
                    end if;

                when TRAN_TO_RX_LNA_TURN_ON =>
                    if rx_indication = '1' then
                        if state_count = 0 then
                            frontend_state <= FRONTEND_STATE_RX;
                        else
                            frontend_state <= TRAN_TO_RX_LNA_TURN_ON;
                        end if;
                    else
                        state_count <= LNA_off_delay;
                        frontend_state <= TRAN_TO_TX_LNA_TURN_OFF;
                    end if;

                when others =>
                    frontend_state <= TRAN_TO_RX_SWITCH_TO_RX;

            end case;
        end if;
    end process break_before_make_proc;

    process(frontend_state)
    begin
        case frontend_state is
            when FRONTEND_STATE_RX =>
                SW_rx <= '1';
                SW_tx <= '0';
                LNA_on <= '1';
                PA_on <= '0';

            when TRAN_TO_TX_LNA_TURN_OFF =>
                SW_rx <= '1';
                SW_tx <= '0';
                LNA_on <= '0';
                PA_on <= '0';

            when TRAN_TO_TX_SWITCH_TO_TX =>
                SW_rx <= '0';
                SW_tx <= '1';
                LNA_on <= '0';
                PA_on <= '0';

            when TRAN_TO_TX_PA_TURN_ON =>
                SW_rx <= '0';
                SW_tx <= '1';
                LNA_on <= '0';
                PA_on <= '1';

            when FRONTEND_STATE_TX =>
                SW_rx <= '0';
                SW_tx <= '1';
                LNA_on <= '0';
                PA_on <= '1';

            when TRAN_TO_RX_PA_TURN_OFF =>
                SW_rx <= '0';
                SW_tx <= '1';
                LNA_on <= '0';
                PA_on <= '0';

            when TRAN_TO_RX_SWITCH_TO_RX =>
                SW_rx <= '1';
                SW_tx <= '0';
                LNA_on <= '0';
                PA_on <= '0';

            when TRAN_TO_RX_LNA_TURN_ON =>
                SW_rx <= '1';
                SW_tx <= '0';
                LNA_on <= '1';
                PA_on <= '0';
        end case;
    end process;
end architecture simple;
